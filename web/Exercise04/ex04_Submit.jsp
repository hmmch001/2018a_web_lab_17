<%--
  Created by IntelliJ IDEA.
  User: hmch001
  Date: 14/05/2018
  Time: 1:13 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Iterator" %>
<html>
<head>
    <title>Server response</title>
</head>
<body>
    <h1>Server side response</h1>
    <p>Thanks for your submission. The values sent to the server are as follows:</p>
    <table>
        <% Map<String, String[]> map = request.getParameterMap();
        Iterator<Map.Entry<String, String[]>> i = map.entrySet().iterator();

        while(i.hasNext()) {
        Map.Entry<String, String[]> entry = i.next();
        String key = entry.getKey();
        String[] values = entry.getValue();

        if(key.contains("submit") || key.contains("button")) {
        continue;
        }

        int index = key.indexOf("[]");
        if(index != -1) {
        key = key.substring(0, index);
        }%>

        <tr><td> <%=key%> </td>

        <%for(String value: values) {%>
        <td><%=value%></td></tr>
        <%}
        }%>
    </table>
</body>
</html>
