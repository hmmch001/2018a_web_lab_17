<%--
  Created by IntelliJ IDEA.
  User: hmch001
  Date: 14/05/2018
  Time: 10:33 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.time.LocalDateTime" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>Exercise 01-02</h1>
<p>Praesent sit amet tincidunt est. Vestibulum quis pulvinar diam, eu auctor massa. Sed efficitur augue nec orci euismod ullamcorper. Etiam lobortis, purus sit amet commodo laoreet, arcu tellus sagittis magna, in scelerisque dolor dui id nibh. Suspendisse ultrices ullamcorper tellus sed consectetur. Vivamus lacus turpis, iaculis sit amet venenatis eget, auctor eget nisi. Duis eget nisi nibh. Aliquam ac metus leo. Nunc euismod convallis volutpat. Morbi commodo eros sit amet purus faucibus rutrum. Proin semper purus nec volutpat euismod. Aliquam nunc nulla, mattis sit amet vulputate ultricies, sollicitudin nec nibh.</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sed porttitor neque. Nulla sagittis libero libero, ac cursus enim semper sed. Donec sed tellus nec libero tincidunt semper. Nam vitae vehicula erat, et rutrum massa. Pellentesque eleifend ligula vitae tellus fringilla accumsan. Nunc tempor vel orci et sodales. Quisque imperdiet quam dolor, at volutpat metus semper sit amet.</p>
<hr>
<%
    LocalDateTime date = LocalDateTime.now();

    out.print("<h2>" + date.toString()+"</h2>");
%>

</body>
</html>
